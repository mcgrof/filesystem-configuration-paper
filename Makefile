all: paper.pdf

TEX_FILES := \
	paper.tex \
	intro_outline.tex \
	abstract.tex *.tex

paper.pdf: paper.tex $(TEX_FILES)
	pdflatex paper.tex

clean:
	rm -f *.aux *.dvi *.log *.out paper.pdf

