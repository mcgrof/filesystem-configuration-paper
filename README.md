About
=====

This paper reviews filesystem configuration files on Linux, with an emphasis
on what we should consider doing for XFS.

Build
======

You should just edit paper.tex and when ready to see your fancy PDF:

	make

To clean things up:

	make clean

Paper
=====

The paper is the file paper.pdf.
