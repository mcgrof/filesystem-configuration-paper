\section{ext2 configuration files}

The ext2 filesystem sported support for a configuration file,
\texttt{mke2fs.conf}, as of e2fsprogs E2FSPROGS-1.39-WIP-1231~14 on
December 31, 2005. Ted confirms the that one of the original motivations and
first use for a configuration file was to fix a long standing bug on sysvinit
2.84-3 reported on Jun 18, 2002, debian bug 150295, "fsck: Please make
\string^C interrupt check"
\footnote{https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=150295}.  Another
primary motivation was to enable Enterprise Linux distribution to disable
certain bleeding edge features, and to enable system administrators to override.
The Debian bug describes the inability to use CTRL-C which triggers a SIGINT signal while fsck
is running, in a reasonable way. If CTRL-C is issued while fsck is running on
ext2 partitions the system would boot into read-only mode even if it was caused
by a routine check.

On April 2003 Theodore Ts'o
suggested this could be handled with e2fsprogs directly, it could enable
gracefully exiting from an fsck check if and only if the triggered check was
due to reaching maximum mount count or maximum time between count is reached
and no errors were detected yet. A possible problem described by enabling this
feature is that other filesystems may not handle SIGINT correctly, and if
\texttt{fsck -A} was used fsck would not know which filesystems to issue a
SIGINT safely for.

A possible solution to this problem is to educate \texttt{fsck} which
filesystems support SIGINT properly. This would then require devising a generic
\texttt{fsck} solution for all filesystems. Another option, which was common
these days for software, was to enable this feature using a \texttt{./configure
--enable-allow-cancellation} option or uncommenting lines in the e2fsprogs
Makefile. This however would constrain users to decisions made by distributions
at configuration and compile time. Disabling this feature by default and
granting the ability for system administrators to enable this functionality
through a configuration file in \texttt{/etc/mke2fs.conf} was a natural
evolution for e2fsprogs. Using a configuration file enabled the flexibility
sought after for users but also provided developers the ability to ensure
sensible and safe defaults were also initially set and used.

The new configuration option to enable forcing cancellation with SIGINT
\texttt{allow\_cancellation} was added upstream after parsing
\texttt{mke2fs.conf} support was added
\footnote{https://git.kernel.org/pub/scm/fs/ext2/e2fsprogs.git/commit/?id=eb065ccf181d49cd1a3709bf607c25d07a6322f1}.
If \texttt{allow\_cancellation} was set to true and a SIGINT issued to
\texttt{fsck.ext2}, and if the filesystem does not have any known problems and
was known to be cleanly unmounted \texttt{fsck.ext2} will exit with a status
code of 0 instead of 32 (FSCK\_CANCELED). This enables bootup to continue
without throwing userspace into a special read-only boot mode. Each filesystem
could implement its own solution as needed.

Other than supporting \texttt{allow\_cancellation} for \texttt{fsck.ext[2-3]},
\texttt{mke2fs.conf} also supports setting default parameters for all
configuration options which you can configure using \texttt{mkfs.ext[2-3]}.

\subsection{mke2fs.conf parsing lineage}

Parsing of the \texttt{mke2fs.conf} file has a pretty unique history and code
lineage. These were the times when fitting e2fsck and mke2fs on 1.44 MB
floppies was still a thing, so reducing binary file size was of essence.
Ted happened to have written a very small, tight file configuration parser
which used an extension of the Windows INI file format for Kerberos V5
\footnote{https://web.mit.edu/kerberos/krb5-1.12/doc/admin/conf\_files/krb5\_conf.html}.
The profile library from MIT Kerberos v5 library was then simplified and
integrated into e2fsprogs
\footnote{https://git.kernel.org/pub/scm/fs/ext2/e2fsprogs.git/commit/?id=2fa9ba98337b07d6acfac52b5ee5dc8116dda866}.
A profile was then added to into e2fsprogs and \texttt{mke2fs.conf} read on
initialization to populate the e2fsprogs profile
\footnote{https://git.kernel.org/pub/scm/fs/ext2/e2fsprogs.git/commit/?id=1017f65179f4aa313f699109b471b79f99f38d3d}.
One of the biggst advantages of e2fsprogs's profile parser is its size. Size
comparisons with other librarires will be provided towards the end of this
document.

The e2fsprogs profile parser is still used to read, parse and manage
\texttt{mke2fs.conf} on e2fsprogs.

\begin{figure}[H]
\begin{lstlisting}[language=C]
[defaults]
    base_features = sparse_super,filetype,resize_inode,dir_index
    blocksize = 4096
    inode_size = 256
    inode_ratio = 16384

[fs_types]
    ext3 = {
	 features = has_journal
    }
    ext4 = {
	 features = extents,flex_bg
	 inode_size = 256
    }
    small = {
	 blocksize = 1024
	 inode_ratio = 4096
    }
    floppy = {
	 features = ^resize_inode
	 blocksize = 1024
	 inode_size = 128
    }
\end{lstlisting}
\caption{/etc/mke2fs.conf file example}
\label{fig:e2fsconf}
\end{figure}
