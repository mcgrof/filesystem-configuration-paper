\section{xfs configuration file - xfs.conf}

The xfs filesystem currently does not support a configuration file, however
work is being done to add support for it. The motivation for adding support
for a configuration file for XFS resembles the ext2 case however it is general.
We'd like the ability to have the option to provide to customers the latest
\texttt{xfsprogs} but ensure that new filesystems created with it will remain
compatible with all intended supported SUSE Linux releases. This can also
easily enable system administrators to create filesystems on new SUSE Linux
releases which are meant to remain compatible with older SUSE Linux releases.
As an an example -- the latest \texttt{mkfs.xfs} creates a filesystem with v5
superblocks if you are on a kernel which supports v5 superblocks. If mounting
this filesystem fails on an older kernel there is nothing in the man pages that
will tell a user how to interpret the error message or worst exactly how to
create a usable file system for that kernel.

As new features get added into the Linux kernel for a xfs,
\texttt{mkfs.xfs} userspace tools will often start enabling these features when
they are sensible and the features are very desirable. When this happens users
of new userspace \texttt{mkfs.xfs} tools could end up creating filesystems
which could no longer be mounted on older kernels. An alternative used today
is to carry patches on top of an an upstream \texttt{xfsprogs} release which
disables specific features by default when known to be incompatible with
older supported kernels. This means carrying patch deltas for each package
for each older distribution. This may also mean some bug fixes need to be
manually cherry picked and backported onto older unsupported releases instead
of just upgrading the latest package for customers.

Currently it is also non-trivial to figure out which are the mkfs.xfs defaults,
and we have no easy way to track when defaults change. A side feature of
supporting a configuration file will also mean having the ability to dump the
\texttt{mkfs.xfs} defaults to a file. With this it will be trivial to be able
to keep track of what defaults change over time, but also to import these
defaults so they can be applied to a newer system so that it creates
filesystems which are backward compatible with older systems.

\subsection{Current XFS default settings}

The \texttt{xfsprogs} code has a SGI/IRIX lineage. The quality of the code
is not up to par with what a Linux developer is used to. One of the shortcomings
revealed in the process of adding configuration file support for XFS is that
today's default settings are only deafult if a parameter is specified but no
value is specified! When no command line parameter is passed it is difficult
to figure out exactly which are the default XFS parameters used -- its all
left to the \texttt{main()} section of \texttt{mkfs/xfs\_mkfs.c}. There are
a lot of options possible, and a lot of options override other options, and
a lot of different ways to compute the value to an option. The best way
to determine today's default options is to use the \texttt{mkfs.xfs -N}
parameter which causes the file system parameters to be printed out without
really creating the file system.

A secondary goal as part of the effort to support a configuration file will
be to make it clear somehow what the defaults are, and also enable to output
what the current settings that would be used for \texttt{mkfs.xfs} would be
used if run, with or without parameters. This should enable generating a
desired configuration file based on command line parameters parsed.

\subsection{Current XFS limitations}

Currently user parameters passed onto \texttt{mkfs.xfs} are parsed at the very
beginning of \texttt{mkfs/xfs\_mkfs.c} \texttt{main()} routine. There are sanity
checks for each possible variable used, however, we currently abort if any
value is invalid, which makes sharing a parameter checker difficult since we'd
like to explain exaclty on what line a configuration file had an issue, with
the current checkers this would not be possible. Additionally XFS uses a global
built-in data structure to set and then check that all parameter can only be
set once. Since the parameter validation mechanism uses this built-in data
structure, supporting a configuration file would currently make configuration
file settings and command line settings mutually exclusive: settings specificed in
a configuration file could not be overridden through the command line, likewise
parameters specified in the command line could not be also specified in the
configuration file. This is obviously very undesirable behaviour, and as such a
lot of code cleanup is being done with \texttt{mkfs/xfs\_mkfs.c} to make
parameter parsing, validation and setting much more flexible and easier to
understand and mantain.  This is a whole project in and of itself. Work on this
front is being done by Jan Tulak at Red Hat.

Once these changes are merged upstream, adding configuration file support will
be much easier and less restrictive.

\subsection{Library requirements}

A library will be used to parse the configuration file instead of reinventing
the wheel. The most important requirements for picking a library is simplicity,
supporting uint64\_t, ensuring most distsributions already carry and
support the library and ensuring there is an active communtiy behind fixing
bugs for the library.

Support for uint64\_t is important given all \texttt{mkfs.xfs} parameters
require a uint64\_t or a bool. To start off with, all parameters configurable
with \texttt{mkfs.xfs} will be tunable, however in the future other parameters
which can also be used for \texttt{fsck.xfs} could be supported in the
configuration file.

XFS's \texttt{mkfs.xfs} built-in data structure for defining parameter
specification and relationships between parameters is expected to be a common
filesystem requirement. A future possible feature from a configuration parser
can be to enable a generic form for specificaion of parameter limitations,
dependencies and relationships.

\subsection{Choosing a parser}

At first glance using the MIT kerberos profile parser to read a configuration
file for XFS seems completely unnecessary for what we need in XFS. Two other
more generic libraries have been evaluated for use: libconfig and
libini\_config. Below is an analysis of each library reviewed.

\subsection{Evaluating libconfig}

Evaluation of libconfig has revealed a long list of bugs, refer to bsc\#1029418
\footnote{https://bugzilla.suse.com/show\_bug.cgi?id=1029418} for details.
It should be made clear that malicious configuration files are not much of a
security topic -- if attackers can modify configuration files, they usually can
do much more damage in other ways than causing integer overflows in the
configuration parser. The issues that have been found then are issues which
must dealt with upstream and each respective project using this library should
consider counter measeures in in lieu of any fixes upstream or consider moving
to another library. Upstream has been notified with a patch fix for one issue,
this patch has been merged. An example code repository has made available
\footnote{https://gitlab.com/mcgrof/libconfig-int64-issues}
which helps reflect the rest of the issues found and possible counter measures.
The remaining issues require major code overhaul.  Packages which could be
affected which use libconfig are listed below.

\texttt{osc whatdependson openSUSE:Factory libconfig standard x86\_64}
\begin{itemize}
	\item \_product:openSUSE-release
	\item compton
	\item compton-conf
	\item fcoe-utils
	\item fedfs-utils
	\item ffado
	\item ffado-mixer
	\item installation-images
	\item jack
	\item kiwi-config-openSUSE
	\item libguestfs
	\item libstoragemgmt
	\item libusbgx
	\item open-lldp
\end{itemize}

\texttt{osci whatdependson SUSE:SLE-12:GA libconfig standard x86\_64}
\begin{itemize}
	\item \_product:SLED-release
	\item \_product:SLES-release
	\item fcoe-utils
	\item ffado
	\item ffado-mixer
	\item installation-images-SLES
	\item jack
	\item libguestfs
	\item open-lldp
\end{itemize}

Although most distributions support this library the current set of issues make
it very unappealing. libconfig also proves to not have any active community
behind it.

\subsection{Evaluating libini\_config}

libini\_config proves to have much more mature code. It was designed for
for The System Security Services Daemon (SSSD), upon review no issues have
been found with it. The libini\_config library has a good set of tests
built into it, and as part of its development model. Its projects inception
was also motivated around ensuring it would have an active community around it.
libini\_config also supports uint64\_t very well.

libini\_config also supports a way to define rules for verifying configuration
file entries. This feature, supported through ini\_rules\_read\_from\_file() and
ini\_rules\_check(), will be evaluated in the future as a way to replace
\texttt{mkfs.xfs}'s currently built-in validator.

A demo is available which tests usigned int and uint64\_t support which
is one of the key requirements for xfs's configuration parser
\footnote{https://gitlab.com/mcgrof/libini\_config-demo}.

\subsection{Evaluating e2fsprogs's profile parser}

Even though the code maturity of libini\_config is very appealing the
e2fsprogs's profile parser size is very small and appealing.

\begin{figure}[H]                                                               
\begin{lstlisting}[language=C] 
$ size -t \
	lib/support/profile.o \
	lib/support/profile_helpers.o \
	lib/support/prof_err.o \
	lib/et/error_message.o \
	lib/et/et_name.o
text    data     bss     dec     hex filename
8842       0       8    8850    2292 lib/support/profile.o
2010       0       0    2010     7da lib/support/profile_helpers.o
1582       0      16    1598     63e lib/support/prof_err.o
2158      16     121    2295     8f7 lib/et/error_message.o
179       0       6     185      b9 lib/et/et_name.o
14771      16     151   14938    3a5a (TOTALS)

$ size  \
	/usr/lib64/libconfig.so.8.3.1 \
	/usr/lib64/libini_config.so.5.2.0
text    data     bss     dec     hex filename
46084    1376       8   47468    b96c /usr/lib64/libconfig.so.8.3.1
100462    2256       8  102726   19146 /usr/lib64/libini_config.so.5.2.0
\end{lstlisting}
\label{fig:sizeojbs}
\end{figure}

The e2fsprogs profile parser is 14.86\% the size of the libini\_config.so.5.2.0!
A stand alone demo has been built with it to help further evaluation
\footnote{https://gitlab.com/mcgrof/libekprofile}. This profile parser however
does not currently support uint64\_t, it however can easily be added.
